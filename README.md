[![size](https://badgen.net/docker/size/tennox/capacitor-android-ci)](https://hub.docker.com/r/tennox/capacitor-android-ci)
[![last commit](https://badgen.net/gitlab/last-commit/txlab/docker-capacitor-android-ci)](https://gitlab.com/txlab/docker-capacitor-android-ci/activity)
[![upstream github](https://badgen.net/badge/icon/gitlab?icon=github&label=upstream)](https://github.com/flashspys/docker-capacitor-android-ci)

# Image to build capacitor android apps

- OpenJDK 8

- rebuilt every week via GitLab CI Schedule
- renovate bot that updates dependencies

## TL;DR Readme

This command exposes an nginx server on port 8080 which serves the folder /absolute/path/to/serve from the host:

```bash
docker run -v $PWD:/app tennox/capacitor-android-ci
```


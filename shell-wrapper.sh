#!/bin/sh

# cat ~/.bashrc

# echo bash -c "source ~/.bashrc;$@"
# set -x
# echo $PATH
# which node
# $NVM_DIR/nvm-exec node -v
# set +x
if grep -q "#DONE" ~/.bashrc; then
    # exec bash -c "source ~/.bashrc; $@"
    exec bash -c "cd $PWD; $NVM_DIR/nvm-exec $@"
else
    exec bash -c "$@"
fi
# Image with node files (this way, renovate updates the tag)
FROM node:16-alpine AS node

FROM thyrlian/android-sdk

# Install OS deps
# RUN apk add --no-cache curl bash shadow
RUN apt-get update && apt-get install -y curl \
    && rm -rf /var/lib/apt/lists/*

# # replace shell with bash so we can source files
# RUN touch ~/.bashrc && chsh -s /bin/bash
# COPY shell-wrapper.sh /shell-wrapper.sh
# SHELL ["/shell-wrapper.sh"]
# # RUN rm /bin/sh && ln -s /bin/bash /bin/sh
# # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/82
# ENV BASH_ENV "/root/.bashrc" 

WORKDIR /root
# RUN export NODE_VERSION="$(cat .node-version | sed 's/v//')"

# Install Node
# RUN curl -fsSL https://fnm.vercel.app/install | bash -s -- --skip-shell \
#   && echo '# fnm' >> ~/.bashrc \
#   && echo 'export PATH=~/.fnm:$PATH' >> ~/.bashrc \
#   && echo 'eval "$(fnm env --shell bash)"' >> ~/.bashrc && . ~/.bashrc && cat ~/.bashrc \
# 	&& eval "$(fnm env --use-on-cd --shell=bash)"
COPY --from=node /usr/lib /usr/lib
COPY --from=node /usr/local/share /usr/local/share
COPY --from=node /usr/local/lib /usr/local/lib
COPY --from=node /usr/local/include /usr/local/include
COPY --from=node /usr/local/bin /usr/local/bin

# # install nvm
# # https://github.com/creationix/nvm#install-script
# ENV NVM_DIR /usr/local/nvm
# RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash
# # install node and npm
# COPY .nvmrc /
# RUN source $NVM_DIR/nvm.sh --no-use; nvm install && nvm use default \
#     && node -v && which node \
#     && echo "export PATH=\$PATH:$(\$ANDROID_SDK_ROOT/build-tools/*/) #DONE" | tee -a ~/.bashrc
# # export PATH=\$PATH:$(dirname $(which node)) 
# # RUN echo ''export PATH=\$PATH:$(dirname $(which node)) 

# Install CLIs
# RUN ls /usr/local/nvm/versions/node/v16.14.0/bin
# RUN /usr/local/nvm/versions/node/v16.14.0/bin/node -v
# RUN echo $PATH
# RUN node -v
RUN npm -g i @quasar/cli @capacitor/cli yarn

RUN mkdir -p /app
WORKDIR /app

# ENTRYPOINT ["/shell-wrapper.sh"]
CMD ["bash"]